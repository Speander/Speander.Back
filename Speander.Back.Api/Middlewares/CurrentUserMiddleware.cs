using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Speander.Back.Core.Interfaces.Services;
using Speander.Back.Domain.Enums.User;

namespace Speander.Back.Api.Middlewares
{
	public class CurrentUserMiddleware
	{
		private readonly RequestDelegate _next;
 
		public CurrentUserMiddleware(RequestDelegate next)
		{
			_next = next;
		}
 
		public async Task InvokeAsync(HttpContext context, ICurrentUserService currentUserService)
		{
			var claims = context.User.Claims.ToArray();
			
			var idClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
			var roleClaim = claims.FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultRoleClaimType);

			if (idClaim != null && !string.IsNullOrEmpty(idClaim.Value))
			{
				currentUserService.Id = Guid.Parse(idClaim.Value);
			}
			
			if (roleClaim != null && !string.IsNullOrEmpty(roleClaim.Value))
			{
				currentUserService.UserRole = Enum.Parse<UserRole>(roleClaim.Value);
			}
			
			await _next.Invoke(context);
		}
	}
}