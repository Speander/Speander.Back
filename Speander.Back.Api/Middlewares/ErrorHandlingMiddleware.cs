using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Speander.Back.Api.Models;

namespace Speander.Back.Api.Middlewares
{
	public class ErrorHandlingMiddleware
	{
		private readonly RequestDelegate _next;
 
		public ErrorHandlingMiddleware(RequestDelegate next)
		{
			_next = next;
		}
 
		public async Task InvokeAsync(HttpContext context)
		{
			try
			{
				await _next.Invoke(context);
			}
			catch (AggregateException e)
			{
				var responseObj = new ApiResponse<object>()
				{
					IsValid = false,
					Errors = e
						.InnerExceptions
						.Select(x => new ApiError(x.Message))
						.ToArray()
				};

				await ResponseException(responseObj, context);
			}
			catch (Exception e)
			{
				var responseObj = new ApiResponse<object>()
				{
					IsValid = false,
					Errors = new []
					{
						new ApiError(e.Message), 
					}
 				};

				await ResponseException(responseObj, context);
			}
		}

		private static async Task ResponseException(ApiResponse<object> obj, HttpContext context)
		{
			var asString = JsonConvert.SerializeObject(obj);

			context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
			context.Response.ContentType = "application/json";
			await context.Response.WriteAsync(asString, Encoding.UTF8);
		}
	}
}