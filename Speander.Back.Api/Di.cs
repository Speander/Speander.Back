using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Speander.Back.Api.Interfaces.Services;
using Speander.Back.Api.Middlewares;
using Speander.Back.Api.Service;

namespace Speander.Back.Api
{
	public static class Di
	{
		public static IServiceCollection InjectApi(this IServiceCollection services)
		{
			services.AddScoped<IValidationService, ValidationService>();

			services.AddAutoMapper(typeof(Di));
			
			return services;
		}
		
		public static IApplicationBuilder UseApi(this IApplicationBuilder builder)
		{
			return builder;
		}
	}
}