using System;
using System.Collections.Generic;
using System.Linq;
using Speander.Back.Api.Dtos.Event;
using Speander.Back.Api.Extensions;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Models.Event
{
	public class EventTimesWrapper : IValidatableForm
	{
		public EventTimeDto[] EventTimes { get; set; }

		public EventTimesWrapper(EventTimeDto[] eventTimes)
		{
			EventTimes = eventTimes;
		}

		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();

			if (EventTimes.Any(x => x.From == default || x.To == default)) 
				exceptions.Add(new Exception("Not all data ranges are correct"));

			var overlap = EventTimes.Overlap();
			if (overlap.Any()) exceptions.Add(new Exception("Ranges overlap detected")); 
			
			return exceptions;
		}
	}
}