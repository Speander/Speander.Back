namespace Speander.Back.Api.Models
{
	public class ApiResponse<T>
	{
		public T Data { get; set; }
		public bool IsValid { get; set; }
		public ApiError[]? Errors { get; set; }

		public ApiResponse()
		{
			Data = default!;
		}
	}
	
	public class ApiResponse
	{
		public object? Data { get; set; }
		public bool IsValid { get; set; }

		public ApiResponse()
		{
			Data = null;
			IsValid = true;
		}
	}
}