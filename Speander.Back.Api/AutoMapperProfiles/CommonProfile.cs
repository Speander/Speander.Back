using AutoMapper;
using Speander.Back.Api.Dtos.Event;
using Speander.Back.Api.Dtos.Geo;
using Speander.Back.Domain.Models.Event;
using Speander.Back.Domain.Models.Geo;

namespace Speander.Back.Api.AutoMapperProfiles
{
	public class CommonProfile : Profile
	{
		public CommonProfile()
		{
			CreateMap<EventDto, Event>();
			CreateMap<Event, EventDto>();
			
			CreateMap<EventLocationDto, EventLocation>();
			CreateMap<EventLocation, EventLocationDto>();
			
			CreateMap<LocationDto, Location>();
			CreateMap<Location, LocationDto>();
			
			CreateMap<EventTime, EventTimeDto>();
			
			CreateMap<EventToken, EventTokenDto>();
			
			CreateMap<EventSpeakerRequestCreationDto, EventSpeakerRequest>();
			CreateMap<EventSpeakerRequest, EventSpeakerRequestDto>();
		}
	}
}