using System;
using System.Collections.Generic;
using System.Linq;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Extensions
{
	public static class RangeExtensions
	{
		public static IEnumerable<IRange<DateTime>[]> Overlap(this IEnumerable<IRange<DateTime>> ranges)
		{
			var last = (IRange<DateTime>?)null;
			
			foreach (var current in ranges.OrderBy(m => m.From))
			{
				if (last != null && OverlapsWith(current, last))
				{
					yield return new [] { last, current };
				}
				
				last = current;
			}
		}

		public static bool OverlapsWith(IRange<DateTime> x, IRange<DateTime> y)
		{
			return x.From < y.From ? x.To > y.From : y.To > x.From;
		} 
	}
}