using System;
using System.Linq;
using Speander.Back.Api.Interfaces;
using Speander.Back.Api.Interfaces.Services;

namespace Speander.Back.Api.Service
{
	internal class ValidationService : IValidationService
	{
		public void Validate<T>(T model) where T : IValidatableForm
		{
			var exceptions = model.Validate();
			if (exceptions == null) return;

			var asArray = exceptions.ToArray();
			
			if (asArray.Any()) throw new AggregateException(asArray);
		}
	}
}