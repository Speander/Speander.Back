using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Speander.Back.Api.Controllers.Base;
using Speander.Back.Api.Dtos.Event;
using Speander.Back.Api.Interfaces.Services;
using Speander.Back.Api.Models;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Api.Controllers
{
	[Authorize]
	[Route("[controller]")]
	public class SpeakerController : BaseController
	{
		private readonly IEventController _eventController;
		private readonly IValidationService _validationService;
		private readonly IMapper _mapper;

		public SpeakerController(
			IMapper mapper, 
			IEventController eventController,
			IValidationService validationService)
		{
			_mapper = mapper;
			_eventController = eventController;
			_validationService = validationService;
		}

		[HttpPost("{tokenId}")]
		public async Task<ApiResponse<EventSpeakerRequestDto>> CreateSpeakerAsync(
			Guid tokenId, 
			[FromBody] EventSpeakerRequestCreationDto dto)
		{
			_validationService.Validate(dto);

			var request = _mapper.Map<EventSpeakerRequest>(dto);
			request = await _eventController.CreateSpeakerRequestAsync(tokenId, request);

			var result = _mapper.Map<EventSpeakerRequestDto>(request);
			return Ack(result);
		}

		[HttpPost("{requestId}/Location/{eventLocationId}")]
		public async Task SetSpeakerRequestTimesAsync(
			Guid requestId,
			Guid eventLocationId,
			[FromBody] SpeakerTimesDto dto)
		{
			_validationService.Validate(dto);
		}
 	}
}