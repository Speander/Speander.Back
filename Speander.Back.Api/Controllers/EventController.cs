using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Speander.Back.Api.Controllers.Base;
using Speander.Back.Api.Dtos.Event;
using Speander.Back.Api.Interfaces.Services;
using Speander.Back.Api.Models;
using Speander.Back.Api.Models.Event;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Domain.Models.Event;
using Speander.Back.Domain.Models.Geo;

namespace Speander.Back.Api.Controllers
{
	[Authorize]
	[Route("[controller]")]
	public class EventController : BaseController
	{
		private readonly IEventController _eventController;
		private readonly IValidationService _validationService;
		private readonly IMapper _mapper;

		public EventController(
			IEventController eventController, 
			IValidationService validationService,
			IMapper mapper)
		{
			_eventController = eventController;
			_validationService = validationService;
			_mapper = mapper;
		}

		[HttpPost]
		public async Task<ApiResponse<EventDto>> CreateAsync([FromBody] EventDto dto)
		{
			_validationService.Validate(dto);

			var @event = _mapper.Map<Event>(dto);

			@event = await _eventController.CreateAsync(@event);

			var result = _mapper.Map<EventDto>(@event);
			
			return Ack(result);
		}
		
		[HttpPost("{eventId}/Location")]
		public async Task<ApiResponse<EventLocationDto>> AddLocationAsync(Guid eventId, [FromBody] EventLocationDto dto)
		{
			_validationService.Validate(dto);

			var eventLocation = new EventLocation
			{
				Location = _mapper.Map<Location>(dto.Location)
			};

			eventLocation = await _eventController.AddEventLocationAsync(eventId, eventLocation);

			var result = new EventLocationDto
			{
				Id = eventLocation.Id,
				LocationId = eventLocation.LocationId
			};

			return Ack(result);
		}

		[HttpPost("{eventId}/Location/{eventLocationId}")]
		public async Task<ApiResponse<EventTimeDto[]>> AddEventTimesAsync(
			Guid eventId, 
			Guid eventLocationId, 
			[FromBody] EventTimeDto[] dtos)
		{
			_validationService.Validate(new EventTimesWrapper(dtos));
			
			var eventTimes = dtos
				.Select(x => new EventTime
				{
					EventLocationId = eventLocationId,
					From = x.From,
					To = x.To
				})
				.ToArray();
			
			eventTimes = await _eventController.SetEventTimesAsync(eventId, eventLocationId, eventTimes);
			
			var result = _mapper.Map<EventTimeDto[]>(eventTimes);
			return Ack(result);
		}

		[HttpPatch("{eventId}/Confirm")]
		public async Task<ApiResponse> SetEventConfirmedAsync(Guid eventId)
		{
			await _eventController.SetEventConfirmedAsync(eventId);
			return EmptyAck();
		}

		[HttpGet("Owned")]
		public async Task<ApiResponse<EventDto[]>> GetOwnedEventsAsync()
		{
			var events = await _eventController.GetAllEventsByCurrentUserAsync();
			var mapped = _mapper.Map<EventDto[]>(events);

			return Ack(mapped);
		}
		
		[HttpGet("{eventId}")]
		public async Task<ApiResponse<EventDto>> GetEventByIdAsync(Guid eventId)
		{
			var @event = await _eventController.GetEventByIdAsync(eventId);
			var mapped = _mapper.Map<EventDto>(@event);
			
			return Ack(mapped);
		}
		
		[Authorize(Roles = "ADMIN")]
		[HttpGet("ToApprove")]
		public async Task<ApiResponse<EventDto[]>> GetEventsToApproveAsync()
		{
			var events = await _eventController.GetAllToApproveAsync();
			var mapped = _mapper.Map<EventDto[]>(events);

			return Ack(mapped);
		}

		[Authorize(Roles = "ADMIN")]
		[HttpPatch("{eventId}/block")]
		public async Task<ApiResponse> BlockAsync(Guid eventId)
		{
			await _eventController.BlockEventAsync(eventId);
			return EmptyAck();
		}
		
		[Authorize(Roles = "ADMIN")]
		[HttpPatch("{eventId}/validate")]
		public async Task<ApiResponse> ValidateAsync(Guid eventId)
		{
			await _eventController.ValidateEventAsync(eventId);
			return EmptyAck();
		}
		
		[HttpPost("{eventId}/tokens")]
		public async Task<ApiResponse<EventTokenDto[]>> CreateTokensAsync(Guid eventId)
		{
			var tokens = await _eventController.GenerateTokensAsync(eventId);
			var mapped = _mapper.Map<EventTokenDto[]>(tokens);

			return Ack(mapped);
		}
		
		[HttpGet("ByToken/{tokenId}")]
		public async Task<ApiResponse<EventDto>> GetEventByTokenAsync(Guid tokenId)
		{
			var @event = await _eventController.GetEventByTokenAsync(tokenId);
			var mapped = _mapper.Map<EventDto>(@event);

			return Ack(mapped);
		}
	}
}