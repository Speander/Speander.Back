using System.Net;
using Microsoft.AspNetCore.Mvc;
using Speander.Back.Api.Models;

namespace Speander.Back.Api.Controllers.Base
{
	public class BaseController : ControllerBase
	{
		[ApiExplorerSettings(IgnoreApi = true)]
		public ApiResponse<T> Ack<T>(T data)
		{
			HttpContext.Response.ContentType = "application/json";
			HttpContext.Response.StatusCode = (int) HttpStatusCode.OK;
			
			var responseObject = new ApiResponse<T>
			{
				Data = data,
				IsValid = true
			};

			return responseObject;
		}

		[ApiExplorerSettings(IgnoreApi = true)]
		public ApiResponse EmptyAck()
		{
			HttpContext.Response.ContentType = "application/json";
			HttpContext.Response.StatusCode = (int) HttpStatusCode.OK;

			return new ApiResponse();
		}
	}
}