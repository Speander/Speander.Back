using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Speander.Back.Api.Controllers.Base;
using Speander.Back.Api.Dtos.Event;
using Speander.Back.Api.Models;
using Speander.Back.Core.Interfaces.Controllers;

namespace Speander.Back.Api.Controllers
{
	[Authorize]
	[Route("[controller]")]
	public class EventTokenController : BaseController
	{
		private readonly IEventController _eventController;
		private readonly IMapper _mapper;

		public EventTokenController(
			IEventController eventController, 
			IMapper mapper)
		{
			_eventController = eventController;
			_mapper = mapper;
		}

		[HttpGet("{tokenId}")]
		public async Task<ApiResponse<EventTokenDto>> GetTokenAsync(Guid tokenId)
		{
			var token = await _eventController.GetTokenByIdAsync(tokenId);
			var mapped = _mapper.Map<EventTokenDto>(token);

			return Ack(mapped);
		}
	}
}