using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Speander.Back.Api.Controllers.Base;
using Speander.Back.Api.Dtos.User;
using Speander.Back.Api.Interfaces.Services;
using Speander.Back.Api.Models;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Domain.Models.User;

namespace Speander.Back.Api.Controllers
{
	[Route("[controller]/[action]")]
	public class UserController : BaseController
	{
		private readonly IValidationService _validationService;
		private readonly IUserController _userController;

		public UserController(
			IValidationService validationService, 
			IUserController userController)
		{
			_validationService = validationService;
			_userController = userController;
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<ApiResponse> Register([FromBody] RegisterDto dto)
		{
			_validationService.Validate(dto);

			var register = new Register
			{
				Email = dto.Email.ToLowerInvariant(),
				Password = dto.Email.ToLowerInvariant()
			};

			await _userController.Register(register);
			
			return EmptyAck();
		}
		
		[HttpPost]
		[AllowAnonymous]
		public async Task<ApiResponse<string>> Login([FromBody] LoginDto dto)
		{
			_validationService.Validate(dto);

			var login = new Login
			{
				Email = dto.Email.ToLowerInvariant(),
				Password = dto.Email.ToLowerInvariant()
			};

			var jwt = await _userController.Login(login);
			return Ack(jwt);
		}
	}
}