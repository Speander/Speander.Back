namespace Speander.Back.Api.Dtos.Geo
{
	public class LocationDto
	{
		public string? Country { get; set; }
		public string? City { get; set; }
		public string? Street { get; set; }
		public string? Building { get; set; }
		public string? Longitude { get; set; }
		public string? Latitude { get; set; }
		public string? Comment { get; set; }
	}
}