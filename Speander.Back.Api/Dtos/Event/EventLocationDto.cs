using System;
using System.Collections.Generic;
using Speander.Back.Api.Dtos.Geo;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventLocationDto : IValidatableForm
	{
		public Guid Id { get; set; }

		public Guid LocationId { get; set; }
		public LocationDto? Location { get; set; }
		public EventTimeDto[]? EventTimes { get; set; }
		
		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();

			if (Location == null) throw new Exception("Location not defined");
			
			return exceptions;
		}
	}
}