using System;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventTokenDto
	{
		public Guid Id { get; set; }
		public EventTokenType Type { get; set; }
	}
}