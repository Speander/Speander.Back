using System;
using System.Collections.Generic;
using Speander.Back.Api.Interfaces;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventDto : IValidatableForm
	{
		public Guid Id { get; set; }
		
		public DateTime CreatedOn { get; set; }
		public EventStatus Status { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		public EventLocationDto[]? EventLocations { get; set; }
		public EventTokenDto[]? EventTokens { get; set; }

		public EventDto()
		{
			Name = string.Empty;
			Description = string.Empty;
		}

		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();
			
			if (string.IsNullOrEmpty(Name)) exceptions.Add(new Exception("Name is not defined"));
			if (string.IsNullOrEmpty(Description)) exceptions.Add(new Exception("Description is not defined"));
			
			return exceptions;
		}
	}
}