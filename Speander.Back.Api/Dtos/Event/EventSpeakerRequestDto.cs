using System;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventSpeakerRequestDto
	{
		public Guid Id { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }
		
		public TimeSpan Duration { get; set; }

		public EventSpeakerRequestStatus Status { get; set; }
	}
}