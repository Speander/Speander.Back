using System;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.Event
{
	public class SpeakerTimeDto : IRange<DateTime>
	{
		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}