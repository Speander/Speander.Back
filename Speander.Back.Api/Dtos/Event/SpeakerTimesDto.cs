using System;
using System.Collections.Generic;
using System.Linq;
using Speander.Back.Api.Extensions;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.Event
{
	public class SpeakerTimesDto : IValidatableForm
	{
		public SpeakerTimeDto[] Times { get; set; }
		
		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();

			if (Times.Any(x => x.From == default || x.To == default)) 
				exceptions.Add(new Exception("Not all data ranges are correct"));

			var overlap = Times.Overlap();
			if (overlap.Any()) exceptions.Add(new Exception("Ranges overlap detected")); 
			
			return exceptions;
		}
	}
}