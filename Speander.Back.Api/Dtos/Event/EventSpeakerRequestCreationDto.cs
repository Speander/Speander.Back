using System;
using System.Collections.Generic;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventSpeakerRequestCreationDto : IValidatableForm
	{
		public string Name { get; set; }
		public string Description { get; set; }
		
		public TimeSpan Duration { get; set; }
		
		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();

			if (string.IsNullOrEmpty(Name)) exceptions.Add(new Exception("Name is not defined"));
			if (string.IsNullOrEmpty(Description)) exceptions.Add(new Exception("Description is not defined"));
			
			if (Duration.Minutes < 5) exceptions.Add(new Exception("Duration can't be lesser than 5 mins"));

			return exceptions;
		}
	}
}