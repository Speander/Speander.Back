using System;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.Event
{
	public class EventTimeDto : IRange<DateTime>
	{
		public Guid Id { get; set; }
		public Guid EventLocationId { get; set; }
		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}