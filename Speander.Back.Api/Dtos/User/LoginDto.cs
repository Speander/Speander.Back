using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Speander.Back.Api.Interfaces;

namespace Speander.Back.Api.Dtos.User
{
	public class LoginDto : IValidatableForm
	{
		private static readonly EmailAddressAttribute _emailAddressAttribute = new EmailAddressAttribute();

		public string Email { get; set; }
		public string Password { get; set; }
		
		public IEnumerable<Exception>? Validate()
		{
			var exceptions = new List<Exception>();
			
			if (string.IsNullOrEmpty(Email)) exceptions.Add(new Exception("Email field is not defined"));
			if (!_emailAddressAttribute.IsValid(Email)) exceptions.Add(new Exception("Email not valid"));
			
			if (string.IsNullOrEmpty(Password)) exceptions.Add(new Exception("Password field is not defined"));

			return exceptions;
		}

		public LoginDto()
		{
			Email = string.Empty;
			Password = string.Empty;
		}
	}
}