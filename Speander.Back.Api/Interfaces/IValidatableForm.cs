using System;
using System.Collections.Generic;

namespace Speander.Back.Api.Interfaces
{
	public interface IValidatableForm
	{
		IEnumerable<Exception>? Validate();
	}
}