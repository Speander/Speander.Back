namespace Speander.Back.Api.Interfaces.Services
{
	public interface IValidationService
	{
		void Validate<T>(T model) where T : IValidatableForm;
	}
}