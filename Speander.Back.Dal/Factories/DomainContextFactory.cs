using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Speander.Back.Dal.Contexts;

namespace Speander.Back.Dal.Factories
{
	public class DomainContextFactory : IDesignTimeDbContextFactory<DomainContext>
	{
		public DomainContext CreateDbContext(string[] args)
		{
			var connString = new ConfigurationBuilder()
				.AddJsonFile("factorySettings.json")
				.Build()
				.GetSection("configurationString")
				.Get<string>();

			var optionsBuilder = new DbContextOptionsBuilder<DomainContext>()
				.UseNpgsql(connString)
				.UseSnakeCaseNamingConvention();

			return new DomainContext(optionsBuilder.Options);
		}
	}
}