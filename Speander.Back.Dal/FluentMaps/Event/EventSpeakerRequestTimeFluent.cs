using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventSpeakerRequestTimeFluent : IEntityTypeConfiguration<EventSpeakerRequestTime>
	{
		public void Configure(EntityTypeBuilder<EventSpeakerRequestTime> builder)
		{
			builder
				.HasOne(x => x.EventLocation)
				.WithMany(x => x.SpeakerRequestTimes)
				.HasForeignKey(x => x.EventLocationId);
			
			builder
				.HasOne(x => x.EventSpeakerRequest)
				.WithMany(x => x.RequestTimes)
				.HasForeignKey(x => x.EventSpeakerRequestId);
		}
	}
}