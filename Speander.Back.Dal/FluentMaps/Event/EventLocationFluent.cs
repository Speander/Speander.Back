using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventLocationFluent : IEntityTypeConfiguration<EventLocation>
	{
		public void Configure(EntityTypeBuilder<EventLocation> builder)
		{
			builder
				.HasOne(x => x.Event)
				.WithMany(x => x!.EventLocations)
				.HasForeignKey(x => x.EventId);
			
			builder
				.HasOne(x => x.Location)
				.WithMany(x => x!.EventLocations)
				.HasForeignKey(x => x.LocationId);
		}
	}
}