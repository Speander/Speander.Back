using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventFluent : IEntityTypeConfiguration<Domain.Models.Event.Event>
	{
		public void Configure(EntityTypeBuilder<Domain.Models.Event.Event> builder)
		{
			builder
				.HasOne(x => x.Creator)
				.WithMany(x => x!.CreatorOf)
				.HasForeignKey(x => x.CreatorId);
		}
	}
}