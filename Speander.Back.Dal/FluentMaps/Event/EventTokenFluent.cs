using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventTokenFluent : IEntityTypeConfiguration<EventToken>
	{
		public void Configure(EntityTypeBuilder<EventToken> builder)
		{
			builder
				.HasOne(x => x.Event)
				.WithMany(x => x.EventTokens)
				.HasForeignKey(x => x.EventId);
		}
	}
}