using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventSpeakerRequestFluent : IEntityTypeConfiguration<EventSpeakerRequest>
	{
		public void Configure(EntityTypeBuilder<EventSpeakerRequest> builder)
		{
			builder
				.HasOne(x => x.Event)
				.WithMany(x => x.EventSpeakerRequests)
				.HasForeignKey(x => x.EventId);
			
			builder
				.HasOne(x => x.Speaker)
				.WithMany(x => x.SpeakerOf)
				.HasForeignKey(x => x.SpeakerId);
		}
	}
}