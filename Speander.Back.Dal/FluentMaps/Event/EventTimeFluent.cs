using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Dal.FluentMaps.Event
{
	public class EventTimeFluent : IEntityTypeConfiguration<EventTime>
	{
		public void Configure(EntityTypeBuilder<EventTime> builder)
		{
			builder
				.HasOne(x => x.EventLocation)
				.WithMany(x => x!.EventTimes)
				.HasForeignKey(x => x.EventLocationId);
		}
	}
}