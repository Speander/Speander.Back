﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Speander.Back.Dal.Migrations
{
    public partial class EventTokenAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "event_tokens",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    event_id = table.Column<Guid>(nullable: false),
                    type = table.Column<int>(nullable: false),
                    revoked = table.Column<bool>(nullable: false),
                    issued_on = table.Column<DateTime>(nullable: false),
                    expired_on = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_event_tokens", x => x.id);
                    table.ForeignKey(
                        name: "fk_event_tokens_events_event_id",
                        column: x => x.event_id,
                        principalTable: "events",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_event_tokens_event_id",
                table: "event_tokens",
                column: "event_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event_tokens");
        }
    }
}
