﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Speander.Back.Dal.Migrations
{
    public partial class EventSpeakerRequestTimeAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "event_speaker_request_times",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    event_speaker_request_id = table.Column<Guid>(nullable: false),
                    event_location_id = table.Column<Guid>(nullable: false),
                    from = table.Column<DateTime>(nullable: false),
                    to = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_event_speaker_request_times", x => x.id);
                    table.ForeignKey(
                        name: "fk_event_speaker_request_times_event_locations_event_location_id",
                        column: x => x.event_location_id,
                        principalTable: "event_locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_event_speaker_request_times_event_speaker_requests_event_spe",
                        column: x => x.event_speaker_request_id,
                        principalTable: "event_speaker_requests",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_event_speaker_request_times_event_location_id",
                table: "event_speaker_request_times",
                column: "event_location_id");

            migrationBuilder.CreateIndex(
                name: "ix_event_speaker_request_times_event_speaker_request_id",
                table: "event_speaker_request_times",
                column: "event_speaker_request_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event_speaker_request_times");
        }
    }
}
