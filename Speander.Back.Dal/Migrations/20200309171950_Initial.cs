﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Speander.Back.Dal.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "locations",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    country = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    street = table.Column<string>(nullable: true),
                    building = table.Column<string>(nullable: true),
                    longitude = table.Column<string>(nullable: true),
                    latitude = table.Column<string>(nullable: true),
                    comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_locations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    password_hash = table.Column<string>(nullable: false),
                    password_salt = table.Column<string>(nullable: false),
                    first_name = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true),
                    birth_date = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    company_name = table.Column<string>(nullable: true),
                    company_position = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: false),
                    phone = table.Column<string>(nullable: true),
                    sex = table.Column<int>(nullable: false),
                    role = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "events",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creator_id = table.Column<Guid>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_events", x => x.id);
                    table.ForeignKey(
                        name: "fk_events_user_creator_id",
                        column: x => x.creator_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "event_locations",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    event_id = table.Column<Guid>(nullable: false),
                    location_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_event_locations", x => x.id);
                    table.ForeignKey(
                        name: "fk_event_locations_events_event_id",
                        column: x => x.event_id,
                        principalTable: "events",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_event_locations_locations_location_id",
                        column: x => x.location_id,
                        principalTable: "locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "event_times",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    event_location_id = table.Column<Guid>(nullable: false),
                    from = table.Column<DateTime>(nullable: false),
                    to = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_event_times", x => x.id);
                    table.ForeignKey(
                        name: "fk_event_times_event_locations_event_location_id",
                        column: x => x.event_location_id,
                        principalTable: "event_locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_event_locations_event_id",
                table: "event_locations",
                column: "event_id");

            migrationBuilder.CreateIndex(
                name: "ix_event_locations_location_id",
                table: "event_locations",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "ix_event_times_event_location_id",
                table: "event_times",
                column: "event_location_id");

            migrationBuilder.CreateIndex(
                name: "ix_events_creator_id",
                table: "events",
                column: "creator_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event_times");

            migrationBuilder.DropTable(
                name: "event_locations");

            migrationBuilder.DropTable(
                name: "events");

            migrationBuilder.DropTable(
                name: "locations");

            migrationBuilder.DropTable(
                name: "user");
        }
    }
}
