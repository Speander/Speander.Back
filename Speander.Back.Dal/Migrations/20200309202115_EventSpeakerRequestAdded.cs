﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Speander.Back.Dal.Migrations
{
    public partial class EventSpeakerRequestAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "event_speaker_requests",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    event_id = table.Column<Guid>(nullable: false),
                    speaker_id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    duration = table.Column<TimeSpan>(nullable: false),
                    status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_event_speaker_requests", x => x.id);
                    table.ForeignKey(
                        name: "fk_event_speaker_requests_events_event_id",
                        column: x => x.event_id,
                        principalTable: "events",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_event_speaker_requests_user_speaker_id",
                        column: x => x.speaker_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_event_speaker_requests_event_id",
                table: "event_speaker_requests",
                column: "event_id");

            migrationBuilder.CreateIndex(
                name: "ix_event_speaker_requests_speaker_id",
                table: "event_speaker_requests",
                column: "speaker_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event_speaker_requests");
        }
    }
}
