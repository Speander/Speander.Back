using System;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Speander.Back.Domain.Models.Event;
using Speander.Back.Domain.Models.Geo;
using Speander.Back.Domain.Models.User;

namespace Speander.Back.Dal.Contexts
{
	public class DomainContext : DbContext
	{
		private static readonly Type[] _usedEnums;
		
		protected DomainContext()
		{
		}

		static DomainContext()
		{
			_usedEnums = Enums();

			var globalMapMethod = typeof(DomainContext)
				.GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
				.Single(x => x.Name == nameof(GloballyMapEnum));

			foreach (var usedEnum in _usedEnums)
			{
				globalMapMethod.MakeGenericMethod(usedEnum).Invoke(null, new object[0]);
			}
		}

		public DomainContext(DbContextOptions options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			var globalMapMethod = typeof(DomainContext)
				.GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
				.Single(x => x.Name == nameof(MapEnum));

			foreach (var usedEnum in _usedEnums)
			{
				globalMapMethod.MakeGenericMethod(usedEnum)
					.Invoke(
						null,
						new object[]
						{
							modelBuilder
						});
			}

			modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
		}

		public DbSet<User>? User { get; set; }
		
		public DbSet<Location>? Locations { get; set; }
		
		public DbSet<Event>? Events { get; set; }
		public DbSet<EventLocation>? EventLocations { get; set; }
		public DbSet<EventTime>? EventTimes { get; set; }
		public DbSet<EventToken>? EventTokens { get; set; }
		public DbSet<EventSpeakerRequest>? EventSpeakerRequests { get; set; }
		public DbSet<EventSpeakerRequestTime>? EventSpeakerRequestTimes { get; set; }

		private static void GloballyMapEnum<TEnum>() where TEnum : struct, Enum
		{
			NpgsqlConnection.GlobalTypeMapper.MapEnum<TEnum>();
		}
		
		private static void MapEnum<TEnum>(ModelBuilder builder) where TEnum : struct, Enum
		{
			builder.HasPostgresEnum<TEnum>();
		}
		
		private static Type[] Enums()
		{
			return new Type[0];
		}
	}
}