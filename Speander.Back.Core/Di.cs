using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Speander.Back.Core.Controllers;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Core.Interfaces.Services;
using Speander.Back.Core.Options;
using Speander.Back.Core.Services.Event;
using Speander.Back.Core.Services.User;

namespace Speander.Back.Core
{
	public static class Di
	{
		public static IServiceCollection InjectCore(this IServiceCollection services, IConfiguration configuration)
		{
			services.Configure<JwtOptions>(configuration.GetSection("Jwt"));
			
			services
				.AddScoped<IUserController, UserController>()
				.AddScoped<ICurrentUserService, CurrentUserService>()
				.AddScoped<IEventController, EventController>();

			services
				.AddScoped<PasswordService>()
				.AddScoped<JwtService>()
				.AddScoped<UserService>()
				.AddScoped<EventService>();

			return services;
		}
		
		public static IApplicationBuilder UseCore(this IApplicationBuilder builder)
		{

			return builder;
		}
	}
}