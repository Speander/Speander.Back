using System;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Core.Models.Event
{
	public class EventPartial
	{
		public EventStatus Status { get; set; }
		public Guid CreatorId { get; set; }
	}
}