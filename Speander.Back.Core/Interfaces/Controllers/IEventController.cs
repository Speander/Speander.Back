using System;
using System.Threading.Tasks;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Core.Interfaces.Controllers
{
	public interface IEventController
	{
		Task<Event> CreateAsync(Event @event);
		Task<EventLocation> AddEventLocationAsync(Guid eventId, EventLocation eventLocation);
		Task<EventTime[]> SetEventTimesAsync(Guid eventId, Guid eventLocationId, EventTime[] eventTimes);
		Task SetEventConfirmedAsync(Guid eventId);
		Task<Event[]> GetAllEventsByCurrentUserAsync();
		Task<Event[]> GetAllToApproveAsync();
		Task<Event> GetEventByIdAsync(Guid eventId);
		Task BlockEventAsync(Guid eventId);
		Task ValidateEventAsync(Guid eventId);
		Task<EventToken[]> GenerateTokensAsync(Guid eventId);
		Task<EventToken> GetTokenByIdAsync(Guid tokenId);
		Task<Event> GetEventByTokenAsync(Guid tokenId);
		Task<EventSpeakerRequest> CreateSpeakerRequestAsync(Guid tokenId, EventSpeakerRequest request);
	}
}