using System.Threading.Tasks;
using Speander.Back.Domain.Models.User;

namespace Speander.Back.Core.Interfaces.Controllers
{
	public interface IUserController
	{
		Task Register(Register register);
		Task<string> Login(Login login);
	}
}