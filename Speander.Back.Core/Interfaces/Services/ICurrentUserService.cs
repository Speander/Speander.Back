using System;
using Speander.Back.Domain.Enums.User;

namespace Speander.Back.Core.Interfaces.Services
{
	public interface ICurrentUserService
	{
		Guid Id { get; set; }
		UserRole UserRole { get; set; }
	}
}