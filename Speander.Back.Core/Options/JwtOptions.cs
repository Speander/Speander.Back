using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Speander.Back.Core.Options
{
	public class JwtOptions
	{
		public string Issuer { get; set; }
		public string Audience { get; set; }
		public string Key { get; set; }
		public long Lifetime { get; set; }

		public SymmetricSecurityKey GetSymmetricSecurityKey()
		{
			return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
		}
	}
}