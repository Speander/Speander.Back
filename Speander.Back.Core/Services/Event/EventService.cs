using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Speander.Back.Core.Models.Event;
using Speander.Back.Dal.Contexts;
using Speander.Back.Domain.Enums.Event;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Core.Services.Event
{
	internal class EventService
	{
		private readonly DomainContext _domainContext;

		public EventService(DomainContext domainContext)
		{
			_domainContext = domainContext;
		}

		public async Task<Domain.Models.Event.Event> SaveAsync(
			Domain.Models.Event.Event @event,
			Guid creatorId)
		{
			@event.Id = Guid.NewGuid();
			@event.CreatorId = creatorId;
			@event.CreatedOn = DateTime.UtcNow;
			@event.Status = EventStatus.CREATED;

			await _domainContext.Events.AddAsync(@event);
			await _domainContext.SaveChangesAsync();

			return @event;
		}

		public async Task<EventLocation> AddEventLocationAsync(Guid eventId, EventLocation eventLocation)
		{
			var eventStatus = await _domainContext
				.Events
				.Where(x => x.Id == eventId)
				.Select(x => x.Status)
				.FirstOrDefaultAsync();
			
			if (eventStatus != EventStatus.CREATED) throw new Exception("You cant change event in this status");

			eventLocation.Id = Guid.NewGuid();
			eventLocation.Location.Id = Guid.NewGuid();
			eventLocation.LocationId = eventLocation.Location.Id;
			eventLocation.EventId = eventId;

			await _domainContext.EventLocations.AddAsync(eventLocation);
			await _domainContext.SaveChangesAsync();

			return eventLocation;
		}

		public async Task<Guid> GetEventCreatorIdAsync(Guid eventId)
		{
			var id = await _domainContext
				.Events
				.Where(x => x.Id == eventId)
				.Select(x => x.CreatorId)
				.FirstOrDefaultAsync();
			
			if (id == default) throw new Exception("Event not found");

			return id;
		}

		public async Task<EventTime[]> SetEventTimesAsync(Guid eventId, Guid eventLocationId, EventTime[] eventTimes)
		{ 
			var eventStatus = await _domainContext
				.Events
				.Where(x => x.Id == eventId)
				.Select(x => x.Status)
				.FirstOrDefaultAsync();
			
			if (eventStatus != EventStatus.CREATED) throw new Exception("You cant change event in this status");
			
			var location = await _domainContext
				.EventLocations
				.Include(x => x.EventTimes)
				.SingleOrDefaultAsync(x => x.EventId == eventId && x.Id == eventLocationId);
			
			if (location == null) throw new Exception("Targeted location not found");
			if (location.EventTimes.Any()) throw new Exception("Location times already set");

			foreach (var eventTime in eventTimes)
			{
				eventTime.Id = Guid.NewGuid();
				eventTime.EventLocationId = eventLocationId;
			}

			await _domainContext.EventTimes.AddRangeAsync(eventTimes);
			await _domainContext.SaveChangesAsync();

			return eventTimes;
		}

		public async Task SetEventConfirmedAsync(Guid eventId)
		{
			var @event = await _domainContext
				.Events
				.Include(x => x.EventLocations).ThenInclude(x => x.EventTimes)
				.FirstOrDefaultAsync(x => x.Id == eventId);
			
			if (@event.Status != EventStatus.CREATED) throw new Exception("You cant change event in this status");
			
			if (@event.EventLocations == null || !@event.EventLocations.Any()) 
				throw new Exception("No locations set for the event");
			
			if (@event.EventLocations.Any(x => x.EventTimes == null || !x.EventTimes.Any()))
				throw new Exception("Not all time ranges set");

			@event.Status = EventStatus.CONFIRMED;
			await _domainContext.SaveChangesAsync();
		}

		public async Task<Domain.Models.Event.Event[]> GetAllEventByUserAsync(Guid userId)
		{
			return await _domainContext
				.Events
				.Where(x => x.CreatorId == userId)
				.OrderByDescending(x => x.CreatedOn)
				.ToArrayAsync();
		}
		
		public async Task<Domain.Models.Event.Event[]> GetAllToApproveAsync()
		{
			return await _domainContext
				.Events
				.Where(x => x.Status == EventStatus.CONFIRMED)
				.OrderByDescending(x => x.CreatedOn)
				.ToArrayAsync();
		}
		
		public async Task<Domain.Models.Event.Event> GetEventByIdAsync(Guid eventId)
		{
			var @event = await _domainContext
				.Events
				.Where(x => x.Id == eventId)
				.Include(x => x.EventLocations).ThenInclude(x => x.EventTimes)
				.Include(x => x.EventLocations).ThenInclude(x => x.Location)
				.Include(x => x.EventTokens)
				.SingleOrDefaultAsync();

			if (@event == null) throw new Exception("Event not found");

			return @event;
		}

		public async Task BlockEventAsync(Guid eventId)
		{
			var @event = await _domainContext
				.Events
				.FirstOrDefaultAsync(x => x.Id == eventId);
			
			if (@event == null) throw new Exception("Event not found");

			@event.Status = EventStatus.BLOCKED;
			await _domainContext.SaveChangesAsync();
		}
		
		public async Task ValidateEventAsync(Guid eventId)
		{
			var @event = await _domainContext
				.Events
				.FirstOrDefaultAsync(x => x.Id == eventId);
			
			if (@event == null) throw new Exception("Event not found");

			@event.Status = EventStatus.VALIDATED;
			await _domainContext.SaveChangesAsync();
		}

		public async Task<EventPartial> GetEventPartialAsync(Guid eventId)
		{
			var result = await _domainContext
				.Events
				.Where(x => x.Id == eventId)
				.Select(
					x => new EventPartial
					{
						Status = x.Status,
						CreatorId = x.CreatorId
					})
				.SingleOrDefaultAsync();
			
			if (result == null) throw new Exception("Event not found");

			return result;
		}

		public async Task<EventToken[]> CreateEventTokensAsync(Guid eventId)
		{
			var now = DateTime.UtcNow;
			var expiration = now.AddYears(1);

			var tokenAlreadyExists = await _domainContext
				.EventTokens
				.Where(x => x.EventId == eventId && !x.Revoked)
				.Select(x => x.Type)
				.ToArrayAsync();
			
			if (tokenAlreadyExists.Contains(EventTokenType.SPEAKER) && tokenAlreadyExists.Contains(EventTokenType.PARTICIPANT))
				throw new Exception("Tokens already exist");
			
			var tokens = new[]
			{
				new EventToken
				{
					Id = Guid.NewGuid(),
					EventId = eventId,
					Type = EventTokenType.SPEAKER,
					IssuedOn = now,
					ExpiredOn = expiration,
					Revoked = false,
				},
				new EventToken
				{
					Id = Guid.NewGuid(),
					EventId = eventId,
					Type = EventTokenType.PARTICIPANT,
					IssuedOn = now,
					ExpiredOn = expiration,
					Revoked = false,
				}
			};

			await _domainContext.EventTokens.AddRangeAsync(tokens);
			await _domainContext.SaveChangesAsync();

			return tokens;
		}

		public async Task<EventToken> GetTokenByIdAsync(Guid tokenId)
		{
			var token = await _domainContext
				.EventTokens
				.SingleOrDefaultAsync(x => x.Id == tokenId);
			
			if (token == null) throw new Exception("Token not found");
			if (token.Revoked || token.ExpiredOn < DateTime.UtcNow) throw new Exception("Token invalid");
			
			return token;
		}

		public async Task<Domain.Models.Event.Event> GetEventByTokenAsync(Guid tokenId)
		{
			var tokenType = await _domainContext
				.EventTokens
				.Where(x => x.Id == tokenId)
				.Select(x => x.Type)
				.FirstOrDefaultAsync();
			
			if (tokenType != EventTokenType.SPEAKER) throw new Exception("Not speaker token");
			
			var @event = await _domainContext
				.Events
				.Include(x => x.EventLocations).ThenInclude(x => x.EventTimes)
				.Include(x => x.EventLocations).ThenInclude(x => x.Location)
				.Where(x => x.EventTokens.Any(e => e.Id == tokenId))
				.SingleOrDefaultAsync();
			
			if (@event == null) throw new Exception("Event not found");

			return @event;
		}

		public async Task<EventSpeakerRequest> CreateSpeakerRequestAsync(Guid tokenId, Guid speakerId, EventSpeakerRequest request)
		{
			var token = await _domainContext
				.EventTokens
				.Where(x => x.Id == tokenId)
				.FirstOrDefaultAsync();
			
			if (token == null) throw new Exception("Token not found");
			if (token.Revoked || token.ExpiredOn < DateTime.UtcNow) throw new Exception("Token invalid");

			request.Status = EventSpeakerRequestStatus.CREATED;
			request.Id = Guid.NewGuid();
			request.SpeakerId = speakerId;
			request.EventId = token.EventId;

			await _domainContext.EventSpeakerRequests.AddAsync(request);
			await _domainContext.SaveChangesAsync();

			return request;
		}
	}
}