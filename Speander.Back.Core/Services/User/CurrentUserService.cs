using System;
using Speander.Back.Core.Interfaces.Services;
using Speander.Back.Domain.Enums.User;

namespace Speander.Back.Core.Services.User
{
	public class CurrentUserService : ICurrentUserService
	{
		public Guid Id { get; set; }
		public UserRole UserRole { get; set; }
	}
}