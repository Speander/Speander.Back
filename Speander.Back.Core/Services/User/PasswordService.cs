using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Speander.Back.Core.Services.User
{
	internal class PasswordService
	{
		public string HashPassword(string password, byte[] salt)
		{
			return Convert.ToBase64String(KeyDerivation.Pbkdf2(
				password: password,
				salt: salt,
				prf: KeyDerivationPrf.HMACSHA512,
				iterationCount: 10000,
				numBytesRequested: 32
			));
		}

		public bool VerifyHashedPassword(string hashedPassword, string providedPassword, string salt)
		{
			var actualHashBytes = Convert.FromBase64String(hashedPassword);
			var saltAsBytes = Convert.FromBase64String(salt);
			
			var expectedHashBytes = KeyDerivation.Pbkdf2(
				password: providedPassword,
				salt: saltAsBytes,
				prf: KeyDerivationPrf.HMACSHA512,
				iterationCount: 10000,
				numBytesRequested: 32
			);

			return CryptographicOperations.FixedTimeEquals(actualHashBytes, expectedHashBytes);
		}
	}
}