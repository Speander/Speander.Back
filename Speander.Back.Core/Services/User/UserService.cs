using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Speander.Back.Dal.Contexts;
using Speander.Back.Domain.Enums.User;
using Speander.Back.Domain.Models.User;

namespace Speander.Back.Core.Services.User
{
	internal class UserService
	{
		private readonly DomainContext _domainContext;
		private readonly PasswordService _passwordService;

		public UserService(
			DomainContext domainContext,
			PasswordService passwordService)
		{
			_domainContext = domainContext;
			_passwordService = passwordService;
		}

		public async Task<Domain.Models.User.User> RegisterAsync(Register register)
		{
			var userWithTheSameLogin = await _domainContext.User.SingleOrDefaultAsync(x => x.Email == register.Email);
			if (userWithTheSameLogin != null) throw new Exception("User with the same login already exists");

			var (passHash, passSalt) = GeneratePassword(register.Password);
			
			var user = new Domain.Models.User.User
			{
				Id = Guid.NewGuid(),
				PasswordHash = passHash,
				PasswordSalt = passSalt,
				Status = UserStatus.CONFIRMED,
				Email = register.Email,
				Role = UserRole.USER
			};

			await _domainContext.User.AddAsync(user);
			await _domainContext.SaveChangesAsync();

			return user;
		}

		public async Task<Domain.Models.User.User> FindAsync(Login login)
		{
			var candidate = await _domainContext.User.SingleOrDefaultAsync(x => x.Email == login.Email);
			if (candidate == null) throw new Exception("User with provided credentials not found");
			
			var passwordMath = _passwordService.VerifyHashedPassword(
				candidate.PasswordHash,
				login.Password,
				candidate.PasswordSalt);
			
			if (!passwordMath) throw new Exception("User with provided credentials not found");
			return candidate;
		}

		private (string hash, string salt) GeneratePassword(string rawPassword)
		{
			var salt = new byte[16];
			
			using var rng = RandomNumberGenerator.Create();
			rng.GetBytes(salt);
			
			var hashed = _passwordService.HashPassword(rawPassword, salt);

			return (hashed, Convert.ToBase64String(salt));
		}
	}
}