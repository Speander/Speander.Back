using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Speander.Back.Core.Options;
using Speander.Back.Domain.Enums.User;

namespace Speander.Back.Core.Services.User
{
	internal class JwtService
	{
		private readonly IOptions<JwtOptions> _jwtOptions;

		public JwtService(IOptions<JwtOptions> jwtOptions)
		{
			_jwtOptions = jwtOptions;
		}

		public string CreateAccessToken(Domain.Models.User.User user)
		{
			var options = _jwtOptions.Value;
			var now = DateTime.UtcNow;
			var identity = GetUserIdentity(user);
			
			var signingCredentials = new SigningCredentials(
				options.GetSymmetricSecurityKey(), 
				SecurityAlgorithms.HmacSha256);
			
			var jwt = new JwtSecurityToken(
				issuer: options.Issuer,
				audience: options.Audience,
				notBefore: now,
				claims: identity.Claims,
				expires: now.Add(TimeSpan.FromSeconds(options.Lifetime)),
				signingCredentials: signingCredentials);
			
			return new JwtSecurityTokenHandler().WriteToken(jwt);
		}
		
		private ClaimsIdentity GetUserIdentity(Domain.Models.User.User user)
		{
			var claims = new []
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
				new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString()),
				new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
			};
			
			var claimsIdentity = new ClaimsIdentity(
				claims, 
				"Token", 
				ClaimsIdentity.DefaultNameClaimType,
				ClaimsIdentity.DefaultRoleClaimType);
			
			return claimsIdentity;
		}
	}
}