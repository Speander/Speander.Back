using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Speander.Back.Dal.Contexts;

namespace Speander.Back.Core.Services.Location
{
	internal class LocationService
	{
		private readonly DomainContext _domainContext;

		public LocationService(DomainContext domainContext)
		{
			_domainContext = domainContext;
		}

		public async Task<Domain.Models.Geo.Location> CreateAsync(Domain.Models.Geo.Location location)
		{
			location.Id = Guid.NewGuid();
			
			await _domainContext.Locations.AddAsync(location);
			await _domainContext.SaveChangesAsync();

			return location;
		}
		
		public async Task<Domain.Models.Geo.Location> GetAsync(Guid id)
		{
			var location = await _domainContext.Locations.SingleOrDefaultAsync(x => x.Id == id);
			if (location == null) throw new Exception("Location not found");

			return location;
		}
	}
}