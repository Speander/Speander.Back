using System.Threading.Tasks;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Core.Services.User;
using Speander.Back.Domain.Models.User;

namespace Speander.Back.Core.Controllers
{
	internal class UserController : IUserController
	{
		private readonly UserService _userService;
		private readonly JwtService _jwtService;

		public UserController(
			UserService userService, 
			JwtService jwtService)
		{
			_userService = userService;
			_jwtService = jwtService;
		}

		public async Task Register(Register register)
		{
			 await _userService.RegisterAsync(register);
		}

		public async Task<string> Login(Login login)
		{
			var user = await _userService.FindAsync(login);
			return _jwtService.CreateAccessToken(user);
		} 
	}
}