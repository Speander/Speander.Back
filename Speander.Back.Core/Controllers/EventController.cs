using System;
using System.Threading.Tasks;
using Speander.Back.Core.Interfaces.Controllers;
using Speander.Back.Core.Interfaces.Services;
using Speander.Back.Core.Services.Event;
using Speander.Back.Domain.Enums.Event;
using Speander.Back.Domain.Enums.User;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Core.Controllers
{
	internal class EventController : IEventController
	{
		private readonly EventService _eventService;
		private readonly ICurrentUserService _currentUserService;

		public EventController(
			EventService eventService,
			ICurrentUserService currentUserService)
		{
			_eventService = eventService;
			_currentUserService = currentUserService;
		}

		public async Task<Event> CreateAsync(Event @event)
		{
			return await _eventService.SaveAsync(@event, _currentUserService.Id);
		}

		public async Task<EventLocation> AddEventLocationAsync(Guid eventId, EventLocation eventLocation)
		{
			if (await _eventService.GetEventCreatorIdAsync(eventId) != _currentUserService.Id)
				throw new Exception("You are not event creator");

			return await _eventService.AddEventLocationAsync(eventId, eventLocation);
		}

		public async Task<EventTime[]> SetEventTimesAsync(Guid eventId, Guid eventLocationId, EventTime[] eventTimes)
		{
			if (await _eventService.GetEventCreatorIdAsync(eventId) != _currentUserService.Id)
				throw new Exception("You are not event creator");

			return await _eventService.SetEventTimesAsync(eventId, eventLocationId, eventTimes);
		}

		public async Task SetEventConfirmedAsync(Guid eventId)
		{
			if (await _eventService.GetEventCreatorIdAsync(eventId) != _currentUserService.Id)
				throw new Exception("You are not event creator");

			await _eventService.SetEventConfirmedAsync(eventId);
		}

		public async Task<Event[]> GetAllEventsByCurrentUserAsync()
		{
			return await _eventService.GetAllEventByUserAsync(_currentUserService.Id);
		}

		public async Task<Event[]> GetAllToApproveAsync()
		{
			return await _eventService.GetAllToApproveAsync();
		}

		public async Task<Event> GetEventByIdAsync(Guid eventId)
		{
			if (_currentUserService.UserRole != UserRole.ADMIN
				&& await _eventService.GetEventCreatorIdAsync(eventId) != _currentUserService.Id)
				throw new Exception("Permission");

			return await _eventService.GetEventByIdAsync(eventId);
		}

		public async Task BlockEventAsync(Guid eventId)
		{
			await _eventService.BlockEventAsync(eventId);
		}

		public async Task ValidateEventAsync(Guid eventId)
		{
			await _eventService.ValidateEventAsync(eventId);
		}

		public async Task<EventToken[]> GenerateTokensAsync(Guid eventId)
		{
			var eventPartial = await _eventService.GetEventPartialAsync(eventId);

			if (eventPartial.CreatorId != _currentUserService.Id)
				throw new Exception("Only creator can generate tokens");
			
			if (eventPartial.Status != EventStatus.VALIDATED) 
				throw new Exception("Event should be validated to create tokens");

			return await _eventService.CreateEventTokensAsync(eventId);
		}

		public async Task<EventToken> GetTokenByIdAsync(Guid tokenId)
		{
			return await _eventService.GetTokenByIdAsync(tokenId);
		}

		public async Task<Event> GetEventByTokenAsync(Guid tokenId)
		{
			return await _eventService.GetEventByTokenAsync(tokenId);
		}

		public async Task<EventSpeakerRequest> CreateSpeakerRequestAsync(Guid tokenId, EventSpeakerRequest request)
		{
			return await _eventService.CreateSpeakerRequestAsync(tokenId, _currentUserService.Id, request);
		}
	}
}