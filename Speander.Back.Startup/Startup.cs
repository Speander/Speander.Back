using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Speander.Back.Api;
using Speander.Back.Api.Middlewares;
using Speander.Back.Core;
using Speander.Back.Core.Options;
using Speander.Back.Dal.Contexts;

namespace Speander.Back.Startup
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			JsonConvert.DefaultSettings = () => new JsonSerializerSettings
			{
				MissingMemberHandling = MissingMemberHandling.Ignore,
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			};

			services
				.InjectApi()
				.InjectCore(Configuration);

			services
				.AddControllers()
				.AddApplicationPart(typeof(Api.Di).Assembly)
				.AddNewtonsoftJson();

			var connectionString = Configuration
				.GetSection("configurationString")
				.Get<string>();

			services
				.AddDbContextPool<DomainContext>(
					options => options
						.UseNpgsql(connectionString)
						.UseSnakeCaseNamingConvention()
						.EnableSensitiveDataLogging());

			services
				.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(
					options =>
					{
						var jwtOptions = Configuration
							.GetSection("Jwt")
							.Get<JwtOptions>();
						
						options.RequireHttpsMetadata = false;
						options.TokenValidationParameters = new TokenValidationParameters
						{
							ValidateIssuer = true,
							ValidIssuer = jwtOptions.Issuer,
							ValidateAudience = true,
							ValidAudience = jwtOptions.Audience,
							ValidateLifetime = true,
							IssuerSigningKey = jwtOptions.GetSymmetricSecurityKey(),
							ValidateIssuerSigningKey = true,
						};
					});
			
			services.AddCors(
				options =>
				{
					options.AddPolicy(
						"Default",
						builder =>
						{
							builder
								.AllowAnyHeader()
								.AllowAnyMethod()
								.AllowAnyOrigin();
						});
				});

			services.AddSwaggerGen(
				c =>
				{
					c.SwaggerDoc(
						"v1",
						new OpenApiInfo
						{
							Title = "Speander.Back",
							Version = "v1"
						});
				});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app
				.UseSwagger()
				.UseSwaggerUI(
					c =>
					{
						c.SwaggerEndpoint("/swagger/v1/swagger.json", "Speander.Back v1");
					});
			
			app
				.UseMiddleware<ErrorHandlingMiddleware>()
				.UseCors("Default")
				.UseRouting()
				.UseAuthentication()
				.UseAuthorization()
				.UseMiddleware<CurrentUserMiddleware>()
				.UseEndpoints(
					endpoints =>
					{
						endpoints.MapControllers();
					});
		}
	}
}