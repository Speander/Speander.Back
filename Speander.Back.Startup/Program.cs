using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Speander.Back.Startup
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			Host
				.CreateDefaultBuilder()
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
					webBuilder.ConfigureLogging((context, builder) => builder.AddConsole());
				})
				.Build()
				.Run();
		}
	}
}