using System;
using System.Collections.Generic;
using Speander.Back.Domain.Enums.User;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Domain.Models.User
{ 
	public class User
	{
		public Guid Id { get; set; }
		public string PasswordHash { get; set; }
		public string PasswordSalt { get; set; }
		public string? FirstName { get; set; }
		public string? LastName { get; set; }
		public DateTime? BirthDate { get; set; }
		public UserStatus Status { get; set; }
		public string? Description { get; set; }
		public string? CompanyName { get; set; }
		public string? CompanyPosition { get; set; }
		public string Email { get; set; }
		public string? Phone { get; set; }
		public Sex Sex { get; set; }
		public UserRole Role { get; set; }

		public ICollection<Event.Event>? CreatorOf { get; set; }
		public ICollection<EventSpeakerRequest>? SpeakerOf { get; set; }

		public User()
		{
			PasswordHash = string.Empty;
			PasswordSalt = string.Empty;
			Email = string.Empty;
		}
	}
}