using System;

namespace Speander.Back.Domain.Models.Event
{
	public class EventTime
	{
		public Guid Id { get; set; }

		public Guid EventLocationId { get; set; }
		public EventLocation? EventLocation { get; set; }

		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}