using System;
using System.Collections.Generic;
using Speander.Back.Domain.Models.Geo;

namespace Speander.Back.Domain.Models.Event
{
	public class EventLocation
	{
		public Guid Id { get; set; }

		public Guid EventId { get; set; }
		public Event? Event { get; set; }

		public Guid LocationId { get; set; }
		public Location? Location { get; set; }

		public ICollection<EventTime>? EventTimes { get; set; }
		public ICollection<EventSpeakerRequestTime>? SpeakerRequestTimes { get; set; }
	}
}