using System;
using System.Collections.Generic;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Domain.Models.Event
{
	public class Event
	{
		public Guid Id { get; set; }

		public Guid CreatorId { get; set; }
		public User.User? Creator { get; set; }

		public DateTime CreatedOn { get; set; }
		public EventStatus Status { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		public ICollection<EventLocation>? EventLocations { get; set; }
		public ICollection<EventToken>? EventTokens { get; set; }
		public ICollection<EventSpeakerRequest>? EventSpeakerRequests { get; set; }
		
		public Event()
		{
			Name = string.Empty;
			Description = string.Empty;
		}
	}
}