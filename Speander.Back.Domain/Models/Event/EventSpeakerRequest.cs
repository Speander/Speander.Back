using System;
using System.Collections.Generic;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Domain.Models.Event
{
	public class EventSpeakerRequest
	{
		public Guid Id { get; set; }

		public Guid EventId { get; set; }
		public Event Event { get; set; }

		public Guid SpeakerId { get; set; }
		public User.User Speaker { get; set; }

		public string Name { get; set; }
		public string Description { get; set; }
		
		public TimeSpan Duration { get; set; }

		public EventSpeakerRequestStatus Status { get; set; }

		public ICollection<EventSpeakerRequestTime>? RequestTimes { get; set; }
	}
}