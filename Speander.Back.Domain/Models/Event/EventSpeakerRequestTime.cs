using System;

namespace Speander.Back.Domain.Models.Event
{
	public class EventSpeakerRequestTime
	{
		public Guid Id { get; set; }

		public Guid EventSpeakerRequestId { get; set; }
		public EventSpeakerRequest EventSpeakerRequest { get; set; }

		public Guid EventLocationId { get; set; }
		public EventLocation EventLocation { get; set; }

		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}