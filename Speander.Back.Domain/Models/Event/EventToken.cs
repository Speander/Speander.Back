using System;
using Speander.Back.Domain.Enums.Event;

namespace Speander.Back.Domain.Models.Event
{
	public class EventToken
	{
		public Guid Id { get; set; }

		public Guid EventId { get; set; }
		public Event Event { get; set; }

		public EventTokenType Type { get; set; }
		public bool Revoked { get; set; }
		public DateTime IssuedOn { get; set; }
		public DateTime ExpiredOn { get; set; }
	}
}