using System;
using System.Collections.Generic;
using Speander.Back.Domain.Models.Event;

namespace Speander.Back.Domain.Models.Geo
{
	public class Location
	{
		public Guid Id { get; set; }
		public string? Country { get; set; }
		public string? City { get; set; }
		public string? Street { get; set; }
		public string? Building { get; set; }
		public string? Longitude { get; set; }
		public string? Latitude { get; set; }
		public string? Comment { get; set; }

		public ICollection<EventLocation>? EventLocations { get; set; }
	}
}