namespace Speander.Back.Domain.Enums.Event
{
	public enum EventSpeakerRequestStatus
	{
		NONE,
		CREATED,
		CONFIRMED,
		APPROVED,
		DECLINED
	}
}