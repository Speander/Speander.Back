namespace Speander.Back.Domain.Enums.Event
{
	public enum EventStatus
	{
		NONE,
		CREATED,
		CONFIRMED,
		VALIDATED,
		CLOSED,
		BLOCKED
	}
}