namespace Speander.Back.Domain.Enums.Event
{
	public enum EventTokenType
	{
		NONE,
		SPEAKER,
		PARTICIPANT
	}
}