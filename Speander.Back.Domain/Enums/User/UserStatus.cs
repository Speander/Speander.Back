namespace Speander.Back.Domain.Enums.User
{
	public enum UserStatus
	{
		NONE,
		REGISTERED,
		CONFIRMED,
		BLOCKED
	}
}