namespace Speander.Back.Domain.Enums.User
{
	public enum UserRole
	{
		NONE,
		USER,
		ADMIN
	}
}