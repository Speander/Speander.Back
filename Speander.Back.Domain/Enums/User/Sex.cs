namespace Speander.Back.Domain.Enums.User
{
	public enum Sex
	{
		NONE,
		MALE,
		FEMALE
	}
}